define windns::records(
  Enum['present', 'absent'] $ensure = 'present',
  Optional[Enum['A', 'CNAME']] $type = undef,
  Optional[String] $data = undef,
  Optional[String] $record = undef,
) {

    # Check if we're allowing a program or port/protocol and validate accordingly
    if $program == undef {
      #check whether to use 'localport', or just 'port' depending on OS
      case $::operatingsystemversion {
        /Windows Server 2003/, /Windows XP/: {
          $local_port_param = 'port'
          unless empty($remote_port) {
            fail "Sorry, :remote_port param is not supported on ${::operatingsystemversion}"
          }
        }
        default: {
          $local_port_param  = 'localport'
          $remote_port_param = 'remoteport'
        }
      }

      $fw_command = 'portopening'

      if $remote_port or $local_port {
        unless $protocol {
          fail 'Sorry, protocol is required, when defining local or remote port'
        }
      }

      if $protocol =~ /^ICMPv(4|6)/ {
        $allow_context = "protocol=${protocol}"
      } else {
        if $local_port {
          $local_port_cmd = "${local_port_param}=${local_port}"
        } else {
          $local_port_cmd = ''
        }

        if $remote_port {
          $remote_port_cmd = "${remote_port_param}=${remote_port}"
        } else {
          $remote_port_cmd = ''
        }

        # Strip whitespace that in case remore_port_cmd is empty
        $allow_context = rstrip("protocol=${protocol} ${local_port_cmd} ${remote_port_cmd}")
      }
    } else {
      $fw_command = 'allowedprogram'
      $allow_context = "program=\"${program}\""
      validate_absolute_path($program)
    }

    # Split the record into domain and record
    $split = split($record, '(.*?)')
    $rec = $split[0]
    $domain = $split[1]
    # Set command to check for existing rules
    # $netsh_exe = "${facts['os']['windows']['system32']}\\netsh.exe"
    $unless= "Get-DnsServerResourceRecord ${rec} -ZoneName ${domain} -ComputerName dc03.esss.lu.se"

    # Use unless for exec if we want the rule to exist, include a description
    if $ensure == 'present' {
      $unless = "Get-DnsServerResourceRecord ${rec} -ZoneName ${domain} -ComputerName dc03.esss.lu.se"
      $onlyif = undef
    } else {
    # Or onlyif if we expect it to be absent; no description argument
      $onlyif = "Get-DnsServerResourceRecord ${rec} -ZoneName ${domain} -ComputerName dc03.esss.lu.se"
      $unless = undef
    }

    if $type == 'A' {
      $dns_command = "Add-DnsServerResourceRecord -A -Name ${rec} -IPv4Address ${data} -ZoneName ${domain} -ComputerName dc03.esss.lu.se"
    } else {
      $dns_command = "Add-DnsServerResourceRecord -CName -Name ${rec} -HostNameAlias ${data} -ZoneName ${domain}"
    }

    exec { "Add record ${record}":
      command  => $dns_command,
      provider => powershell,
      onlyif   => $onlyif,
      unless   => $unless,
    }
}
